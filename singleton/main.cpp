#include <iostream>

#include "singleton.h"

class MySingleTest {
private:
    std::string name;

public:
    MySingleTest(const MySingleTest& other) : name(other.name) {
        std::cout << "MySingleTest 左值copy构造函数" << std::endl;
    }

    MySingleTest(const MySingleTest&& other) : name(other.name) {
        std::cout << "MySingleTest 右值copy构造函数" << std::endl;
    }

    MySingleTest(const std::string& n) : name(n) {
        std::cout << "MySingleTest 左值构造函数" << std::endl;
    }

    MySingleTest(const std::string&& n) : name(n) {
        std::cout << "MySingleTest 右值构造函数" << std::endl;
    }

    void PrintName() {
        std::cout << name << std::endl;
    }

    ~MySingleTest() {
        std::cout << "MySingleTest 析构函数" << std::endl;
    }
};

int main() {
    // 在主线程中调用 Singleton<T>::New 和  Singleton<T>::Delete
    // 其他地方调用 Singleton<T>::Get() 即可

    // new出来一个对象的单例，存在Move语义，则执行move语义
    // 临时对象，并未显示创建
    std::cout << "-------1111------\n";
    Singleton<MySingleTest>::New("123");

    std::cout << "-------2222------\n";
    // 调用对象的成员函数
    Singleton<MySingleTest>::Get()->PrintName();

    std::cout << "-------3333------\n";
    // 删除对象
    // 不会调用123的析构函数，会在函数结束之后，调用123的析构函数
    Singleton<std::string>::Delete();

    std::cout << "-------4444------\n";
    // 显示创建一份对象
    MySingleTest myclass("1234");
    std::cout << "-------5555------\n";
    //   Singleton<MySingleTest>::New(std::move(myclass));
    Singleton<MySingleTest>::New(myclass);
    std::cout << "-------6666------\n";
    Singleton<MySingleTest>::Get()->PrintName();
    // 会删除myclass对象，并调用其析构函数
    Singleton<MySingleTest>::Delete();
}