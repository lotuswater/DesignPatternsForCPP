// 单例模式 工业级
// 参考 OneFlow 和 B站 “不想吃糖liao” 的视频
#include <iostream>

template <typename T>
class Singleton final {
public:
    static T* Get() { return *GetPPtr(); }

    template <typename... Args>
    static void New(Args&&... args) {
        if (Get() == nullptr) {
            *GetPPtr() = new T(std::forward<Args>(args)...);
        }
    }

    static void Delete() {
        if (Get() != nullptr) {
            delete Get();
            *GetPPtr() = nullptr;
            // std::cout << "删除对象" << std::endl;
        }
    }

private:
    static T** GetPPtr() {
        static T* ptr = nullptr;
        return &ptr;
    }
};
