#include <iostream>

template <typename T>
class Singleton final {
public:
    static T& GetInstance() {
        static T instance;
        return instance;
    }

private:
    Singleton() = default;
    Singleton(const Singleton& ) = delete;
    Singleton(Singleton&& ) = delete;

    Singleton& operator=(const Singleton& ) = delete;
    Singleton& operator=(Singleton&& ) = delete;
};

class MySingleClass {
private:
    std::string name;

public:
    MySingleClass() : name("Hello Singleton!") {
        std::cout << "MySingleClass 无参构造函数" << std::endl;
    }

    MySingleClass(const MySingleClass& other) : name(other.name) {
        std::cout << "MySingleClass 左值构造函数 2" << std::endl;
    }

    MySingleClass(const MySingleClass&& other) : name(other.name) {
        std::cout << "MySingleClass 右值构造函数 2" << std::endl;
    }

    MySingleClass(const std::string& n) : name(n) {
        std::cout << "MySingleClass 左值构造函数" << std::endl;
    }

    MySingleClass(const std::string&& n) : name(n) {
        std::cout << "MySingleClass 右值构造函数" << std::endl;
    }

    void PrintName() {
        std::cout << name << std::endl;
    }

    ~MySingleClass() {
        std::cout << "MySingleClass 调用析构函数" << std::endl;
    }
};

class MySingleClass2 {
private:
    std::string name;

public:
    MySingleClass2() : name("Hello Singleton 2!") {
        std::cout << "MySingleClass2 无参构造函数" << std::endl;
    }

    MySingleClass2(const MySingleClass2& other) : name(other.name) {
        std::cout << "MySingleClass2 左值构造函数 2" << std::endl;
    }

    MySingleClass2(const MySingleClass2&& other) : name(other.name) {
        std::cout << "MySingleClass2 右值构造函数 2" << std::endl;
    }

    MySingleClass2(const std::string& n) : name(n) {
        std::cout << "MySingleClass2 左值构造函数" << std::endl;
    }

    MySingleClass2(const std::string&& n) : name(n) {
        std::cout << "MySingleClass2 右值构造函数" << std::endl;
    }

    void PrintName() {
        std::cout << name << std::endl;
    }

    ~MySingleClass2() {
        std::cout << "MySingleClass2 调用析构函数" << std::endl;
    }
};

int main() {
    std::cout << "-------1111------\n";
    Singleton<MySingleClass>::GetInstance();
    Singleton<MySingleClass2>::GetInstance();

    std::cout << "-------2222------\n";
    // 调用对象的成员函数
    Singleton<MySingleClass>::GetInstance().PrintName();

    std::cout << "-------3333------\n";
    Singleton<MySingleClass2>::GetInstance().PrintName();

    std::cout << "-------4444------\n";
    Singleton<MySingleClass>::GetInstance().PrintName();

    return 0;
}
